﻿open System.IO
open Day1
open Day2
open Day3
open Day4
open Day5

let solutions = dict [1, solveDay1; 2, solveDay2; 3, solveDay3;
                      4, solveDay4; 5, solveDay5]

[<EntryPoint>]
let main argv =
    let dayNumber =
        match argv.Length with
        | 0 -> solutions.Keys |> Seq.max
        | _ -> int argv.[0]
    let inputsPath = sprintf "Inputs/%i.txt" dayNumber
    let input = File.ReadLines(inputsPath)
    solutions.Item dayNumber input
    0
