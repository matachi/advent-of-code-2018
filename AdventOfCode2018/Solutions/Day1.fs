module Day1

let solveDay1 (input: seq<string>) =
    let frequencyChanges =
        input
        |> Seq.map int
    let answerA =
        frequencyChanges
        |> Seq.sum
    printfn "Answer A: %i" answerA

    let repeatForever sequence =
        let l = Seq.toList sequence
        Seq.initInfinite int
        |> Seq.map (fun y -> l.[y % l.Length])
    let cumulativeSums sequence =
        (Seq.scan (+) 0 sequence)
    let incrementValueInMap (map: Map<'T, int>) (key: 'T) =
        match map.ContainsKey key with
        | true -> map.Add(key, (map.Item key + 1))
        | false -> map.Add(key, 1)
    let answerB =
        frequencyChanges
        |> repeatForever
        |> cumulativeSums
        |> (fun sum -> Seq.scan (fun (_, map) key -> key, incrementValueInMap map key) (0, Map.empty) sum)
        |> Seq.skipWhile (fun (_, map) -> map.Count = 0)
        |> Seq.where (fun (key, map) -> map.Item(key) > 1)
        |> Seq.map (fun (key, map) -> key)
        |> Seq.item 0
    printfn "Answer B: %i" answerB