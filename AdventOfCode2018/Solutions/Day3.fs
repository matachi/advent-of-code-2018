module Day3

type Coordinate = {
    X: int
    Y: int
}
type Size = {
    Width: int
    Height: int
}
type Claim = {
    Id: int
    Coordinate: Coordinate
    Size: Size
}

let solveDay3 (input: seq<string>) =
    let stringToClaim (str: string) =
        let split =
            str.Split [|' '; '#'; ','; ':'; '@'; 'x'|]
            |> Array.where (fun str -> str <> "")
            |> Array.map int
        {Id = split.[0]
         Coordinate = {X = split.[1]
                       Y = split.[2]}
         Size = {Width = split.[3]
                 Height = split.[4]}}
    let coveredCoordinatesByClaim claim =
        seq {
            for x in [claim.Coordinate.X..claim.Coordinate.X+claim.Size.Width-1] do
                for y in [claim.Coordinate.Y..claim.Coordinate.Y+claim.Size.Height-1] do
                    yield ({X = x; Y = y}, claim)
        }
    let claims =
        input
        |> Seq.map stringToClaim
    let coveredCoordinates =
        claims
        |> Seq.collect coveredCoordinatesByClaim
        |> Seq.groupBy (fun (coordinate, _) -> coordinate)
        |> Seq.map (fun (coordinate, claims) ->
            (coordinate, claims |> Seq.map (fun (_, claim) -> claim)))
        |> Map.ofSeq
    let claimsOnTheSameSpotAtLeastTwice =
        coveredCoordinates
        |> Map.toSeq
        |> Seq.where (fun (_, claims) -> claims |> Seq.length >= 2)
        |> Seq.map (fun (_, claims) -> claims)
    printfn "Answer A: %i" (Seq.length claimsOnTheSameSpotAtLeastTwice)

    let overlappingClaims =
        claimsOnTheSameSpotAtLeastTwice
        |> Seq.collect id
        |> Set
    let nonOverlapingClaim =
        claims
        |> Seq.where (fun claim -> Set.contains claim overlappingClaims |> not)
        |> Seq.item 0
    printfn "Answer B: %i" nonOverlapingClaim.Id
