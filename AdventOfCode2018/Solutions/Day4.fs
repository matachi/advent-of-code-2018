module Day4
open System

type GuardEntry = {
    Time: DateTime
    Entry: string
}

let solveDay4 (input: seq<string>) =
    let stringToGuardEntry (str: string) =
        let split =
            str.Split([|"["; "] "|], StringSplitOptions.None)
        {Time = DateTime.Parse split.[1]
         Entry = split.[2]}
    let guardEntries =
        input
        |> Seq.map stringToGuardEntry
        |> Seq.sortBy (fun entry -> entry.Time)
        |> Seq.toList
    let guardEntriesToTimeSleptMap (guardEntries: seq<GuardEntry>) =
        let mutable map = Map.empty
        let mutable guardId = 0
        let mutable startTime = DateTime.MinValue
        for row in guardEntries do
            let split = row.Entry.Split()
            if split.[0] = "Guard" then
                guardId <- int (split.[1].Substring 1)
            elif split.[0] = "falls" then
                startTime <- row.Time
            elif split.[0] = "wakes" then
                let mutable minutes = List.empty
                let mutable minute =
                    if startTime.Minute < row.Time.Minute  then startTime.Minute
                    else 0
                while minute < row.Time.Minute do
                    minutes <- minute :: minutes
                    minute <- minute + 1
                map <-
                    if map.ContainsKey guardId then
                        map.Add(guardId, map.Item guardId @ minutes)
                    else
                        map.Add(guardId, minutes)
        map
    let timeSleptMap = guardEntriesToTimeSleptMap guardEntries
    // Find the guard that slept the most
    let (guardId, minutes) =
        timeSleptMap
        |> Map.toSeq
        |> Seq.maxBy (fun (_, minutes) -> minutes.Length)
    // Find the minute where the guard slept the most
    let (mostSleptMinute, _) =
        minutes
        |> List.countBy id
        |> Seq.maxBy (fun (_, sleptNumberOfTimesAtMinute) -> sleptNumberOfTimesAtMinute)
    let answerA = guardId * mostSleptMinute
    printfn "Answer A: %i" answerA

    let (guardThatSleptTheMostOnTheSameMinute, (minute, _)) =
        timeSleptMap
        |> Map.toSeq
        |> Seq.map (fun (guardId, minutes) ->
            (guardId,
             minutes
             |> List.countBy id
             |> Seq.maxBy (fun (_, numberOfTimes) -> numberOfTimes)))
        |> Seq.maxBy (fun (_, (_, sleptNumberOfTimesAtMinute)) -> sleptNumberOfTimesAtMinute)
    let answerB = guardThatSleptTheMostOnTheSameMinute * minute
    printfn "Answer B: %i" answerB
