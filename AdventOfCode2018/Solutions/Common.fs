module Common

let charsToString (chars: seq<char>) =
    chars
    |> Seq.map string
    |> String.concat ""

let applyFunctionWhileOutputChanges (f: 'a -> 'a) (input: 'a) : 'a =
    let mutable tempInput = input
    let mutable output = f tempInput
    while tempInput <> output do
        tempInput <- output
        output <- f tempInput
    output

let concatenatePairs (pairs: list<'a * 'a>) : list<'a> =
    let (firstElement, _) = Seq.head pairs
    [firstElement] @ (pairs |> List.map (fun (_, b) -> b))

let rec removePair (condition: 'a * 'a -> bool) (items: list<'a>) : list<'a> =
    match items.Length with
    | 0 | 1 -> items
    | _ ->
        if condition (items.Head, items.Tail.Head) then items.Tail.Tail
        else items.Head :: removePair condition items.Tail
