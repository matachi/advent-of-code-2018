module Day2
open Common

let solveDay2 (input: seq<string>) =
    let stringToHistogram (str: string) =
        str.ToCharArray()
        |> Seq.groupBy id
        |> Map.ofSeq
        |> Map.map (fun _ v -> Seq.length v)
    let mapHasValueEqualsTo (value: 'U) (map: Map<'T, 'U>) : bool =
        Map.toSeq map
        |> Seq.map (fun (_, v) -> v)
        |> Seq.contains value
    let histograms =
        input
        |> Seq.map (fun (str: string) -> str, stringToHistogram str)
        |> Map.ofSeq
    let stringsWithSameLetterTwice =
        input
        |> Seq.where (fun x -> histograms.Item x |> mapHasValueEqualsTo 2)
        |> Seq.length
    let stringsWithSameLetterTrice =
        input
        |> Seq.where (fun x -> histograms.Item x |> mapHasValueEqualsTo 3)
        |> Seq.length
    printfn "Answer A: %i" (stringsWithSameLetterTwice * stringsWithSameLetterTrice)

    let numberOfDifferingCharacters (str1: string) (str2: string) =
        str1
        |> Seq.zip str2
        |> Seq.where (fun (x, y) -> x <> y)
        |> Seq.length
    let differByOneCharacter str1 str2 =
        (numberOfDifferingCharacters str1 str2) = 1
    let findPairOfMatchingElements (sequence: seq<string>) comparator =
        let list = Seq.toList sequence
        list
        |> List.allPairs list
        |> List.where (fun x -> x ||> comparator)
        |> List.item 0
    let commonElements seq1 seq2 =
        seq1
        |> Seq.zip seq2
        |> Seq.where (fun x -> x ||> (=))
        |> Seq.map (fun (x, y) -> x)
    let answerB =
        findPairOfMatchingElements input differByOneCharacter
        ||> commonElements
        |> charsToString
    printfn "Answer B: %s" answerB
