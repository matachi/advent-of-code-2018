module Day5
open Common
open System

let solveDay5 (input: seq<string>) =
    let polymer = Seq.item 0 input
    let doReaction (polymer: string) : string =
        polymer.ToCharArray()
        |> Array.toList
        |> removePair (fun (a, b) -> a <> b && Char.ToLower a = Char.ToLower b)
        |> charsToString
    let answerA =
        polymer
        |> applyFunctionWhileOutputChanges doReaction
        |> String.length
    printfn "Answer A: %i" answerA
